package com.kantinan.w10;

public class Rectangle extends Shaspe{
    private double width;
    private double height;
    public Rectangle(double width , double height){
        super("Rectangle");
        this.height = height;
        this.width = width;
    }
    public double getWidth(){
        return width;
    }
    public double getHeight(){
        return height;
    }
    @Override
    public String toString(){
        return this.getName() + "  width : " + this.width + "  height : " + this.height;
    }
    @Override
    public double calArea(){
        return this.width + this.height;
    }
    @Override
    public double calPerimeter(){
        return this.width*2 + this.height*2;
    }
}
