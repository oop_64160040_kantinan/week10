package com.kantinan.w10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec = new Rectangle(5,3);
        System.out.println(rec);
        System.out.printf("%s area : %.3f \n",rec.getName(), rec.calArea());
        System.out.printf("%s perimeter : %.3f \n",rec.getName(), rec.calArea());

        Rectangle rec2 = new Rectangle(2,2);
        System.out.println(rec2);
        System.out.printf("%s area : %.3f \n",rec2.getName(), rec2.calArea());
        System.out.printf("%s perimeter : %.3f \n",rec2.getName(), rec2.calArea());

        Circle circle = new Circle(2);
        System.out.println(circle);
        System.out.printf("%s area : %.3f \n",circle.getName(), circle.calArea());
        System.out.printf("%s perimeter : %.3f \n",circle.getName(), circle.calPerimeter());

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%s area : %.3f \n",circle2.getName(), circle2.calArea());
        System.out.printf("%s perimeter : %.3f \n",circle2.getName(), circle2.calPerimeter());

        Triangle triangle = new Triangle(2, 2, 2);
        System.out.println(triangle);
        System.out.printf("%s area : %.3f \n",triangle.getName(), triangle.calArea());
        System.out.printf("%s perimeter : %.3f \n",triangle.getName(), triangle.calPerimeter());
    }
}
