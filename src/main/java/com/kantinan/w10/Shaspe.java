package com.kantinan.w10;

public abstract class Shaspe {
    private String name;
    public Shaspe(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }

    public abstract double calArea();
    public abstract double calPerimeter();
}
